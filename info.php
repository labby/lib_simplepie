<?php

/**
 *  @module      	Library SimplePie
 *  @version        see info.php of this module
 *  @author         cms-lab
 *  @copyright      2010-2023 cms-lab
 *  @license        see license included in the package
 *  @license terms  see info.php of this addon
 *  @platform       see info.php of this addon
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$module_directory = 'lib_simplepie';
$module_name      = 'Library SimplePie';
$module_function  = 'library';
$module_version   = '1.8.0.1';
$module_platform  = '7.x';
$module_delete	  =  true;
$module_author    = 'cms-lab';
$module_license   = '<a href="https://cms-lab.com/_documentation/lib-simplepie/license.php" target="_blank">Addon License</a>  | <a href="https://cms-lab.com/_documentation/lib-simplepie/readme.php" target="_blank"> Read me</a>';
$module_license_terms   = '-';
$module_description = 'This module installs basic files <a href="https://simplepie.org/" target="_blank">Simple Pie</a>.';
$module_guid      = '26b3891e-0cf1-49c5-87ad-18708dcd7439';
$module_home      = 'https://cms-lab.com';
