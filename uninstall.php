<?php

/**
 *  @module      	Library SimplePie
 *  @version        see info.php of this module
 *  @author         cms-lab
 *  @copyright      2010-2023 cms-lab
 *  @license        see license included in the package
 *  @license terms  see info.php of this addon
 *  @platform       see info.php of this addon
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$droplet_names = array(
	'simplepie-details',
	'simple_pie_detail',
	'simplepie-list',
	'simple_pie_list'
);
LEPTON_handle::uninstall_droplets($droplet_names);

LEPTON_handle::register( "rm_full_dir" );
rm_full_dir( LEPTON_PATH.PAGES_DIRECTORY.'/cache'; )
