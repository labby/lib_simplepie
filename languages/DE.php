<?php

/**
 *  @module      	Library SimplePie
 *  @version        see info.php of this module
 *  @author         cms-lab
 *  @copyright      2010-2023 cms-lab (http://cms-lab.com)
 *  @license        see license included in the package
 *  @license terms  see info.php of this addon
 *  @platform       see info.php of this addon
 */

$MOD_LIB_SIMPLEPIE = array(
    "posted_when" => "Gesendet: "
);