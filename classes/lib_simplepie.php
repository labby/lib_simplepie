<?php

/**
 *  @module      	Library SimplePie
 *  @version        see info.php of this module
 *  @author         cms-lab
 *  @copyright      2010-2023 cms-lab (http://cms-lab.com)
 *  @license        see license included in the package
 *  @license terms  see info.php of this addon
 *  @platform       see info.php of this addon
 */

require_once LEPTON_PATH."/modules/lib_simplepie/library.php";

class lib_simplepie extends LEPTON_abstract
{

    public static $instance;

    public $cacheLocation = LEPTON_PATH.PAGES_DIRECTORY."/cache";
    
    public $dateFormat = 'j F Y | g:i a';
    
    public $forceCSStoHead = true;
    private $bCSSinHead = false;
    
	// Inherited function from LEPTON_abstract.
    public function initialize()
    {
        if(!file_exists($this->cacheLocation))
        {
            LEPTON_core::make_dir( $this->cacheLocation );
        }
        
        $this->dateFormat = DEFAULT_DATE_FORMAT." - ".DEFAULT_TIME_FORMAT;
    }

	// function list output
    public function feed_list( $max = '10', $url = '' )
	{
		// Set which feed to process.
		$feed = new SimplePie();
		$feed->set_feed_url('feed://'.$url);

        $feed->set_cache_location( $this->cacheLocation );

		// Run SimplePie.
		$feed->init();

        $feed_values = array(
            "feed_title"    => $feed->get_title(),
            "feed_link"     => $feed->get_permalink(),
            "feed_description"  => $feed->get_description(),
            "feed_items"    => array()
        );

		// This makes sure that the content is sent to the browser as text/html and the UTF-8 character set (since we didn't change it).
		$feed->handle_content_type();

		$count=0;
		foreach ($feed->get_items() as $item){
             $feed_values['feed_items'][] = array(
                "title"         => $item->get_title(),
                "permalink"     => $item->get_permalink(),
                "description"   => $item->get_description(),
                "date"          => $item->get_date( $this->dateFormat ) 
            );
            if(++$count >= $max)
            {
                break;
            }
		}

        $oTWIG = lib_twig_box::getInstance();
        $oTWIG->registerModule("lib_simplepie");
        
		$html = $oTWIG->render(
		    "@lib_simplepie/list.lte",
		    $feed_values
		);
		
		$html .= $this->CSStoTead();

		unset( $feed);
				
		return $html;		
				
	}

	// function detailed output
    public function feed_details( $max = '10', $url = '' )
	{
		// Set which feed to process.
		$feed = new SimplePie();
		$feed->set_feed_url('feed://'.$url);

        $feed->set_cache_location( $this->cacheLocation );

		// Run SimplePie.
		$feed->init();

        $feed_values = array(
            "feed_title"    => $feed->get_title(),
            "feed_link"     => $feed->get_permalink(),
            "feed_description"  => $feed->get_description(),
            "feed_items"    => array()
        );

		// This makes sure that the content is sent to the browser as text/html and the UTF-8 character set (since we didn't change it).
		$feed->handle_content_type();

		$count=0;
		foreach ($feed->get_items() as $item){
            $feed_values['feed_items'][] = array(
                "title"         => $item->get_title(),
                "permalink"     => $item->get_permalink(),
                "description"   => $item->get_description(),
                "date"          => $item->get_date( $this->dateFormat ) 
            );
            if(++$count >= $max)
    		{
    		    break;
    		}
		}

        $oTWIG = lib_twig_box::getInstance();
        $oTWIG->registerModule("lib_simplepie");
        
		$html = $oTWIG->render(
		    "@lib_simplepie/details.lte",
		    $feed_values
		);
		
		$html .= $this->CSStoTead();
		
        unset( $feed);
				
		return $html;		
	}
	
	private function CSStoTead()
	{
	    if( true === $this->forceCSStoHead )
	    { 
	        if(false === $this->bCSSinHead)
	        {
                
                $oLEPTON = LEPTON_frontend::getInstance();
                $current_template = $oLEPTON->page['template'] != "" ? $oLEPTON->page['template'] : DEFAULT_TEMPLATE;
                $sLookUpPath = "/templates/".$current_template."/frontend/lib_simplepie/frontend.css";
                $sCSSPath = file_exists( LEPTON_PATH.$sLookUpPath )
                    ? $sLookUpPath
                    : "/modules/lib_simplepie/css/frontend.css"
                    ;
                    
                $oTWIG = lib_twig_box::getInstance();
	            // $oTWIG->registerModule("lib_simplepie");
	            
	            $this->bCSSinHead = true;
	            
	            return $oTWIG->render(
	                "@lib_simplepie/css2head.lte",
	                array(
	                    "LEPTON_URL"    => LEPTON_URL,
	                    "CSSPath"       => $sCSSPath
	                )
	            );
	        }
	        return "";
	    }
	    return "";
	}
	
}
