<?php

/**
 *  @module      	Library SimplePie
 *  @version        see info.php of this module
 *  @author         cms-lab
 *  @copyright      2010-2023 cms-lab
 *  @license        see license included in the package
 *  @license terms  see info.php of this addon
 *  @platform       see info.php of this addon
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

// create mandantory cache directory
// LEPTON_handle::register( "make_dir" );
$cache_path = LEPTON_PATH.PAGES_DIRECTORY.'/cache';
LEPTON_core::make_dir($cache_path); 

$content = ''.
"<?php

/**
 *  @module      	Library SimplePie
 *  @version        see info.php of this module
 *  @author         cms-lab
 *  @copyright      2010-2023 cms-lab
 *  @license        see license included in the package
 *  @license terms  see info.php of this addon
 *  @platform       see info.php of this addon
*/

header('Location: ../');
?>";

$handle = fopen($cache_path.'/index.php', 'w');
fwrite($handle, $content);
fclose($handle);
LEPTON_core::change_mode($cache_path.'/index.php', 'file');

//	install droplets
$zip_names = array(
	'droplet_simplepie-details',
	'droplet_simplepie-list'
);
LEPTON_handle::install_droplets('lib_simplepie',$zip_names);

//	remove install directory
LEPTON_handle::delete_obsolete_directories('/modules/lib_simplepie/install');

