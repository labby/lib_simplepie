<?php

/**
 *  @module      	Library SimplePie
 *  @version        see info.php of this module
 *  @author         cms-lab
 *  @copyright      2010-2023 cms-lab
 *  @license        see license included in the package
 *  @license terms  see info.php of this addon
 *  @platform       see info.php of this addon
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

// if directory not exists, create mandantory cache directory
$directory = LEPTON_PATH.PAGES_DIRECTORY.'/cache';
if(!file_exists($directory)) {
	LEPTON_handle::register( "make_dir" );
	make_dir($directory); 

	$content = ''.
"<?php

/**
 *  @module      	Library SimplePie
 *  @version        see info.php of this module
 *  @author         cms-lab
 *  @copyright      2010-2023 cms-lab
 *  @license        see license included in the package
 *  @license terms  see info.php of this addon
 *  @platform       see info.php of this addon
*/

header('Location: ../');
?>";

	$handle = fopen(LEPTON_PATH.PAGES_DIRECTORY.'/cache/index.php', 'w');
	fwrite($handle, $content);
	fclose($handle);
	change_mode(LEPTON_PATH.PAGES_DIRECTORY.'/cache/index.php', 'file');		
}

//uninstall old droplets
$droplet_names = [
	'simple_pie_detail',
	'simple_pie_list'
];
LEPTON_handle::uninstall_droplets($droplet_names);

//	install new droplets
$zip_names = [
	'droplet_simplepie-details',
	'droplet_simplepie-list'
];
LEPTON_handle::install_droplets('lib_simplepie',$zip_names);

//	remove install directory
$dirs = [
	'/modules/lib_simplepie/install',
	'/modules/lib_simplepie/idn',
	'/modules/lib_simplepie/library'
];
LEPTON_handle::delete_obsolete_directories($dirs);
